# spark

#### 介绍
图解Spark代码

#### 章节架构
本书的具体包括：Spark概述及入门实战、Spark内核原理、Spark SQL、DataFrame、DataSet原理和实战、Spark数据源、流式计算原理和实战、亿级数据处理平台Spark性能调优、Spark机器学习库、Spark3.0新特性和数据湖。内容丰富详实，简单易懂。期望以最简单的方式聊透复杂的Spark内核原理。
章节架构
本书共8章，各章所讲内容如下。
第1章为Spark概述及入门实战。具体包括：Spark简介、Spark原理及特点、Spark入门实战。其中Spark简介包括：为什么要学习Spark？学好Spark的关键。Spark原理及特点包括：Spark核心优势、Spark生态介绍、Spark模块组成、Spark运行模式、Spark集群组角色、Spark核心概念、Spark作业运行流程。Spark入门实战包括：Spark Standalone环境安装实战、YARN环境安装实战、Spark 批处理作业入门实战和Spark流式作业入门实战。
第2章为Spark的作业调度和资源分配算法。具体包括：Spark的作业调度、Spark On YARN 资源调度、Spark RDD概念、Spark RDD 分区、Spark RDD依赖关系、Spark Stage原理、Spark RDD 持久化、Spark RDD检查点、Spark RDD实战。其中Spark的作业调度包括：Spark 作业运行框架概述、Spark 调度器原理、Spark 应用程序核心概念、Spark 应用程序调度流程、Spark作业YARN 级别调度、Spark作业任务级别调度（FIFO调度策略和FAIR 调度策略）、Spark本地化化调度、Spark延迟调度策略、Spark 失败重试与黑名单机制、Spark推测式执行和Spark的资源分配机制。Spark On YARN 资源调度包括：Spark On YARN运行模式（客户端模式和集群模式）、YARN 调度器（YARN FIFO 调度器、YARN Capacity 调度器、YARN Fair 调度器）、Spark RDD实战包括：Spark RDD Transformation和Action的操作、Spark RDD懒加载机制、Spark 广播变量的概念和使用等内容。
第3章为Spark SQL、DataFrame、DataSet原理和实战。具体包括：Spark SQL基础概念、创建一个 Spark SQL 应用、Spark SQL视图操作、Spark DataSet操作、park DataFrame 操作、Spark SQL 表关联操作和Spark SQL 函数操作等内容。
第4章为深入理解Spark数据源。具体包括：Spark文件读写原理、Spark数据格式介绍、Spark读写HDFS、Spark读写HBase、Spark读写MongoDB、Spark读写Cassandra、Spark读写MySQL、Spark读写Kafka、Spark读写ElasticSearch。其中Spark数据格式包括：Text、CSV、JSON、Parquet、ORC、AVRO。
第5章为流式计算原理和实战。具体包括：Spark Streaming原理和实战、Spark Structured Streaming原理和实战。其中Spark Streaming原理和实战包括：Spark Streaming介绍和入门实战、Spark Streaming 的数据源、DStream。Spark Structured Streaming原理和实战包括：Spark Structured Streaming 原理、特点和数据模型、如何创建一个 Spark Structured Streaming 应用、Spark Structured Streaming时间概念、延迟数据处理策略、和容错语义、Spark Structured Stearming 编程模型和Spark Structured Streaming的时间窗口操作、延迟数据处理、水印、重复数据处理等内容。
第6章为亿级数据处理平台Spark性能调优。具体包括：Spark内存调优、任务调优、数据本地性调优、算子调优、Spark SQL调优、Spark Shuffle调优、Spark Streaming调优、Spark数据倾斜问题处理。其中Spark内存调优包括：JVM新生代、老年代、元数据区的特点和内存调优设置，堆外内存设置，storageFraction设置和Spark JVM调优的最佳实践。任务调优包括：Spark内存及CPU配置、Spark并行度调优、任务等待时长调优、黑名单调优、数据本地性调优、检查点、算子调优。
第7章为Spark机器学习库。具体包括：Spark机器学习概述、Spark机器学习常用统计方法、Spark分类模型、协同过滤、Spark聚类模型。其中Spark分类模型包括：线性回归、逻辑回归、朴素贝叶斯、决策树。Spark聚类模型包括：K-Means算法模型和K-Means++算法模型。
第8章为Spark3.0新特性和数据湖。具体包括：Spark 3.0 新特性、Spark 未来趋势-数据湖。其中Spark 3.0 新特性包括：Spark3.0自适应查询执行（AQE）、Spark SQL3.0新特性、Koalas发布和增强的PySpark和数据湖等。其中Spark SQL3.0新特性包括：动态合并 Shuffle分区、动态调整表关联策略、动态优化倾斜的表关联操作、动态分区裁剪和Join hints等内容。Spark未来趋势-数据湖包括：为什么需要数据湖以及数据湖方案Delta Lake的原理和实战。
